<?php

/**
 * @category Bitbull
 * @package  Bitbull_DefaultShipping
 * @author   Gennaro Vietri <gennaro.vietri@bitbull.it>
 */
class Bitbull_DefaultShipping_Model_Observer
{
    /**
     * Retrieve shopping cart model object
     *
     * @return Mage_Checkout_Model_Cart
     */
    protected function _getCart()
    {
        return Mage::getSingleton('checkout/cart');
    }

    public function setCheapestShippingMethod(Varien_Event_Observer $observer)
    {
        //durante il login non si devono fare operazioni sul quote & address
        //se non vengono fuori errori di Integrity constraint violation sulla tabella sales_flat_quote_shipping_rate
        $pageIdentifier = Mage::app()->getFrontController()->getAction()->getFullActionName();

        if ($pageIdentifier == "customer_account_loginPost") {
            return $this;
        } else {

            /* @var $quote Mage_Sales_Model_Quote */
            $quote = $observer->getData('quote');

            /* @var $quoteAddress Mage_Sales_Model_Quote_Address */
            $quoteAddress = $quote->getShippingAddress();

            $chosenCountryId = Mage::helper('bitbull_defaultshipping')->getCountryId();

            if ($quoteAddress->getAddressType() == Mage_Sales_Model_Quote_Address::TYPE_SHIPPING
                && null === $quoteAddress->getCountryId()
                && null !== $chosenCountryId
            ) {

                $quoteAddress
                    ->setCountryId($chosenCountryId);

                $this->_getCart()
                    ->init()
                    ->save();

                // Recupero le shipping rate per individuare la più economica
                $cheapestRateCode = '';
                $cheapestRatePrice = null;

                $groups = $quoteAddress->getGroupedAllShippingRates();

                foreach ($groups as $rateGroup) {
                    if (!empty($rateGroup)) {
                        foreach ($rateGroup as $rate) {
                            if (null === $cheapestRatePrice || (float)$rate->getPrice() < (float)$cheapestRatePrice) {
                                $cheapestRatePrice = (float)$rate->getPrice();
                                $cheapestRateCode = $rate->getCode();
                            }
                        }
                    }
                }

                if (!empty($cheapestRateCode)) {
                    $quoteAddress->setShippingMethod($cheapestRateCode);
                }
            }


            return $this;
        }
    }
}